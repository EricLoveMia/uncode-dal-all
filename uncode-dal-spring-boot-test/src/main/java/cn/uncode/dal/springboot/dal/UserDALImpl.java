package cn.uncode.dal.springboot.dal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import cn.uncode.dal.external.AbstractCommonDAL;
import cn.uncode.dal.springboot.dto.User;
import cn.uncode.dal.springboot.mapper.UserMapper;

@Service
public class UserDALImpl extends AbstractCommonDAL<User> implements UserDAL {
	
	@Autowired
	@Lazy
	private UserMapper userMapper;

	@Override
	public User user() {
		return userMapper.users();
	}

	
}
