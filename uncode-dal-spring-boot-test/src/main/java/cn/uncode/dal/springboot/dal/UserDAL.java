package cn.uncode.dal.springboot.dal;

import cn.uncode.dal.external.CommonDAL;
import cn.uncode.dal.springboot.dto.User;

public interface UserDAL extends CommonDAL<User> {
	
	User user();

}
