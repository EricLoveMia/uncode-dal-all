package cn.uncode.dal.springboot.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.uncode.dal.springboot.biz.UserService;
import cn.uncode.dal.springboot.dto.User;

/**
 * Created by KevinBlandy on 2017/2/28 15:51
 */
@RestController
@RequestMapping(value = "user")
public class UserController {
	
	@Autowired
	private HttpServletRequest request;

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "list",method = RequestMethod.GET)
	public User users(){
		return userService.users();
	}
	
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public User add(){
		userService.create(null);
		return null;
	}
	
	@RequestMapping(value = "addlist", method = RequestMethod.GET)
	public User addlist(){
		userService.addList(null);
		return null;
	}
	
	@RequestMapping(value = "upd", method = RequestMethod.GET)
	public User upd(){
		
		userService.update(null);
		return null;
	}
	
	@RequestMapping(value = "del", method = RequestMethod.GET)
	public User del(){
		userService.delete(14);
		return null;
	}
	
	@RequestMapping(value = "sel", method = RequestMethod.GET)
	public User sel(){
		request.getRemoteHost();
		
		System.out.println(request.getRemoteAddr());
		System.out.println(request.getRemoteHost());
		System.out.println(request.getRemotePort());
		System.out.println(request.getProtocol());
		String header = request.getHeader("user-Agent");
        System.out.println(header);
		
		return userService.get(1);
	}
	
}
