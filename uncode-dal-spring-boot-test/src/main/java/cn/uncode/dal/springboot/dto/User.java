package cn.uncode.dal.springboot.dto;

import cn.uncode.dal.core.BaseDTO;

/**
 * Created by KevinBlandy on 2017/2/28 15:08
 */
public class User extends BaseDTO{
	
	private String name;
	
	private int age;
	
	private Group group;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}


	
}
	
