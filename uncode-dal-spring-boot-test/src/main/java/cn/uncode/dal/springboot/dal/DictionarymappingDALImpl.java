package cn.uncode.dal.springboot.dal;

import org.springframework.stereotype.Service;

import cn.uncode.dal.external.AbstractCommonDAL;
import cn.uncode.dal.springboot.dto.Dictionarymapping;

 /**
 * service类,此类由Uncode自动生成
 * @author uncode
 * @date 2019-04-29
 */
@Service
public class DictionarymappingDALImpl extends AbstractCommonDAL<Dictionarymapping> implements DictionarymappingDAL {

}