package cn.uncode.dal.springboot.configuration;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cn.uncode.dal.cache.CacheManager;
import cn.uncode.dal.cache.support.SimpleCacheManager;
import cn.uncode.dal.core.BaseDAL;
import cn.uncode.dal.datasource.DynamicDataSource;
import cn.uncode.dal.descriptor.db.ResolveDataBase;
import cn.uncode.dal.descriptor.db.impl.SimpleResolveDatabase;
import cn.uncode.dal.jdbc.datasource.DataSourceTransactionManager;
import cn.uncode.dal.mybatis.CommonMapper;
import cn.uncode.dal.mybatis.MybatisDAL;
import cn.uncode.dal.springboot.cache.DALRedisCache;
import cn.uncode.dal.springboot.config.UncodeDALConfig;

/**
 * Created by KevinBlandy on 2017/2/28 14:11
 */
@Configuration
@EnableConfigurationProperties({UncodeDALConfig.class})
@ConditionalOnClass({DynamicDataSource.class, SqlSessionFactory.class})
public class UncodeDALAutoConfiguration {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UncodeDALAutoConfiguration.class);
	
	@Autowired
	private UncodeDALConfig uncodeDALConfig;
	
	
	@Bean(name = "commonMapper")
	@ConditionalOnMissingBean
	public CommonMapper commonMapper(SqlSessionFactory sqlSessionFactory){
		MapperFactoryBean<CommonMapper> mapperFactoryBean = new MapperFactoryBean<CommonMapper>();
		mapperFactoryBean.setMapperInterface(CommonMapper.class);
		mapperFactoryBean.setSqlSessionFactory(sqlSessionFactory);
		sqlSessionFactory.getConfiguration().addMapper(CommonMapper.class);
		CommonMapper commonMapper = null;
		try {
			commonMapper = (CommonMapper)mapperFactoryBean.getObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.info("===Uncode-Dal===UncodeDALAutoConfiguration===>CommonMapper inited..");
		return commonMapper;
	}
	
	@Bean(name = "dalCacheManager")
	public CacheManager cacheManager(){
		CacheManager cacheManager = new SimpleCacheManager();
		cacheManager.setCache(new DALRedisCache());
		LOGGER.info("===Uncode-Dal===UncodeDALAutoConfiguration===>CacheManager inited..");
		return cacheManager;
	}
	
	
	@Bean(name = "resolveDatabase")
	@ConditionalOnMissingBean
	public ResolveDataBase resolveDatabase(DataSource uncodeDataSource){
		SimpleResolveDatabase simpleResolveDatabase = new SimpleResolveDatabase();
		simpleResolveDatabase.setDataSource(uncodeDataSource);
		LOGGER.info("===Uncode-Dal===UncodeDALAutoConfiguration===>ResolveDataBase inited..");
		return simpleResolveDatabase;
	}
	
	
	@Bean(name = "baseDAL")
	@ConditionalOnMissingBean
	public BaseDAL baseDAL(CommonMapper commonMapper,  CacheManager cacheManager, ResolveDataBase resolveDatabase){
		MybatisDAL mybatisDAL = new MybatisDAL();
		mybatisDAL.setCommonMapper(commonMapper);
		mybatisDAL.setCacheManager(cacheManager);
		mybatisDAL.setResolveDatabase(resolveDatabase);
		mybatisDAL.setUseCache(uncodeDALConfig.isUseCache());
		mybatisDAL.setUseCacheFilter(uncodeDALConfig.isUseCacheFilter());
		mybatisDAL.setNoCacheTables(uncodeDALConfig.getNoCacheTables());
		mybatisDAL.setCacheTables(uncodeDALConfig.getCacheTables());
		mybatisDAL.setVersion(uncodeDALConfig.getVersionField());
		mybatisDAL.setVersionTables(uncodeDALConfig.getVersionTables());
		LOGGER.info("===Uncode-Dal===UncodeDALAutoConfiguration===>BaseDAL inited..");
		return mybatisDAL;
	}
	
	@Bean(name = "transactionManager")
	@ConditionalOnMissingBean
	public DataSourceTransactionManager transactionManager(DataSource uncodeDataSource){
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
		transactionManager.setDataSource(uncodeDataSource);
		LOGGER.info("===Uncode-Dal===UncodeDALAutoConfiguration===>TransactionManager inited..");
		return transactionManager;
	}
}
