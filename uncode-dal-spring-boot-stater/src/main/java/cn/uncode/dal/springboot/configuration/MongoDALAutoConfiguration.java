package cn.uncode.dal.springboot.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mongodb.DB;
import com.mongodb.client.MongoDatabase;

import cn.uncode.dal.mongo.Mongo3DAL;
import cn.uncode.dal.mongo.MongoDataBase;
import cn.uncode.dal.springboot.config.MongoDALConfig;


/**
 * Created by KevinBlandy on 2017/2/28 14:11
 */
@Configuration
@EnableConfigurationProperties({MongoDALConfig.class})
@ConditionalOnClass({MongoDatabase.class, DB.class})
public class MongoDALAutoConfiguration {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MongoDALAutoConfiguration.class);
	
	@Autowired
	private MongoDALConfig mongoDALConfig;
	
	@Bean(name = "mongoDataBase")
	public MongoDataBase mongoDataBase(){
		MongoDataBase mongoDataBase = new MongoDataBase();
		mongoDataBase.setHost(mongoDALConfig.getHost());
		mongoDataBase.setPort(mongoDALConfig.getPort());
		mongoDataBase.setDb(mongoDALConfig.getDb());
		mongoDataBase.setUsername(mongoDALConfig.getUsername());
		mongoDataBase.setPassword(mongoDALConfig.getPassword());
		LOGGER.info("===Uncode-Dal===MongoDALAutoConfiguration===>MongoDataBase inited..");
		return mongoDataBase;
	}
	
	
	@Bean(name = "mongo3DAL")
	public Mongo3DAL mongo3DAL(MongoDataBase mongoDataBase){
		Mongo3DAL mongo3DAL = new Mongo3DAL();
		mongo3DAL.setDatabase(mongoDataBase);
		LOGGER.info("===Uncode-Dal===MongoDALAutoConfiguration===>Mongo3DAL inited..");
		return mongo3DAL;
	}
	
}
