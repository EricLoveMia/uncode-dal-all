package cn.uncode.dal.utils;

import org.apache.commons.lang3.StringUtils;

import cn.uncode.dal.criteria.QueryCriteria;

public class UncodeUtil {
	public static QueryCriteria.Criteria addNumNoEmpty(QueryCriteria.Criteria criteria, String prop, Number number){
		if(criteria== null || isEmpty(number)){
			return criteria;
		}
		criteria.andColumnEqualTo(prop, number);
		return criteria;
	}
	public static QueryCriteria.Criteria addStrNoBlank(QueryCriteria.Criteria criteria, String prop, String str){
		if(criteria== null || StringUtils.isNotBlank(str)){
			return criteria;
		}
		criteria.andColumnEqualTo(prop, str);
		return criteria;
	}
	
	
	
    // 下面是数字处理
    
    /**
     * @param number 判断基本数据类型或者包装类是否为null或者0
     * @return boolean
     */
	public static boolean isEmpty(Number number){
		return number == null || number.doubleValue() == 0;
	}

    /**
     * @param number 判断基本数据类型或者包装类是否不为null或者不为0
     * @return boolean
     */
	public static boolean isNotEmpty(Number number){
		return !isEmpty(number);
	}
	
}
