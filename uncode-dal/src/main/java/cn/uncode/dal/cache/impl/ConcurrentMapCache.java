package cn.uncode.dal.cache.impl;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.uncode.dal.cache.Cache;

public class ConcurrentMapCache implements Cache {
	
	private static final Logger LOG = LoggerFactory.getLogger(ConcurrentMapCache.class);

    private static final ConcurrentMap<Object, Object> cache = new ConcurrentHashMap<Object, Object>();

    @Override
    public int getSize(String tableSpace) {
      return cache.size();
    }

    @Override
    public void putObject(String tableSpace, String key, Object value) {
      cache.put(key, value);
      LOG.debug("Storing " + key + " to cache.");
    }

    @Override
    public Object getObject(String tableSpace, String key) {
    	Object value = cache.get(key);
    	if(LOG.isDebugEnabled()){
    		if(value == null){
    			LOG.debug(key + " cache not exists.");
        	}else{
        		LOG.debug("Reading " + key + " from cache.");
        	}
    	}
    	return value;
    }

    @Override
    public Object removeObject(String tableSpace, String key) {
    	LOG.debug("Removing " + key + " from cache.");
    	return cache.remove(key);
    }

    @Override
    public void clear(String tableSpace, String id) {
        Iterator<Object> iter = cache.keySet().iterator();
        while(iter.hasNext()){
            String key = String.valueOf(iter.next());
            if(key.indexOf(id) != -1){
            	LOG.debug("Clearing " + key + " from cache.");
                cache.remove(key);
            }
        }
        LOG.debug("Clearing *" + id + "* from cache.");
    }

	@Override
	public void putObject(String tableSpace, String key, Object value, int seconds) {
		putObject(tableSpace, key, value);
		LOG.debug("Storing " + key + " to cache[seconds:"+seconds+"].");
	}


}
